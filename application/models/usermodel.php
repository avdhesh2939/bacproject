<?php

class Usermodel extends CI_Model {

	public function submitQuery($arr){

				$date=new datetime();
			$c_date=$date->format('d-m-Y H:i:s');
			$arr['datetime']=$c_date;

			$q=$this->db->insert('contact',$arr);
			if($this->db->affected_rows()){
				return TRUE; 
			}else{
				return FALSE;
			}
			}

	public function registerVendor($arr){

			$date=new datetime();
		$c_date=$date->format('d-m-Y H:i:s');
		$arr['datetime']=$c_date;

		$q=$this->db->insert('vendors',$arr);
		if($this->db->affected_rows()){
			return TRUE; 
		}else{
			return FALSE;
		}
		}

	public function get_category(){

		$q=$this->db->get('category');
		if($q->num_rows()){

			 $row=$q->result();
			 return $row;
		}
		else{
			return FALSE;
		}
	}

	public function get_projects(){
		$this->db->select('*,p.id as pid,p.datetime as d');
		$this->db->from('projects as p');
		$this->db->join('category', 'category.id = p.cat_id');
		$q = $this->db->get();
		
		if($q->num_rows()){

			 $row=$q->result();
			 return $row;
		}
		else{
			return FALSE;
		}
	}

	public function get_projectById($id){

		$this->db->select('*,p.id as pid,p.datetime as d');
		$this->db->from('projects as p');
		$this->db->join('category', 'category.id = p.cat_id');
		$this->db->where('p.id',$id);
		$q = $this->db->get();
		if($q->num_rows()){

			 $row=$q->row_array();
			 return $row;
		}
		else{
			return FALSE;
		}
	}

	public function login_valid( $username, $password )
	{
		$q = $this->db->where(['username'=>$username,'password'=>$password])
						->get('user');

		if ( $q->num_rows() ) {
			return $q->row()->id;
		} else {
			return FALSE;
		}
	}

	public function insert_category($arr){
		$date=new datetime();
		$c_date=$date->format('d-m-Y H:i:s');
		$arr['datetime']=$c_date;

		$q=$this->db->insert('category',$arr);
		if($this->db->affected_rows()){
			return TRUE; 
		}else{
			return FALSE;
		}
	}

	

	public function delete_category($id){

		$q=$this->db->delete('category',["id"=>$id]);
		return TRUE;
	
	}

	public function get_catById($id){

		$q=$this->db->select('*')
					->from('category')
					->where(['id'=>$id])
					->get();
		if($q->num_rows()){

			 $row=$q->row_array();
			 return $row;
		}
		else{
			return FALSE;
		}
	}
	
	public function update_catById($arr){

		$q=$this->db->set('cat_name', $arr['cat_name'])
				 	->where('id', $arr['id'])
				 	->update('category');
		if($q){

			 
			 return TRUE;
		}
		else{
			return FALSE;
		}
	}

	public function insert_project($arr){
		$date=new datetime();
		$c_date=$date->format('d-m-Y H:i:s');
		$arr['datetime']=$c_date;

		$q=$this->db->insert('projects',$arr);
		if($this->db->affected_rows()){
			return TRUE; 
		}else{
			return FALSE;
		}
	}

	

	public function delete_project($id){

		$q=$this->db->delete('projects',["id"=>$id]);
		return TRUE;
	
	}

	

	public function update_projectById($arr){

		$q=$this->db->set('type', $arr['type'])
					->set('cat_id', $arr['cat_id'])
					->set('name', $arr['name'])
					->set('description', $arr['description'])
					->set('link', $arr['link'])
				 	->where('id', $arr['id'])
				 	->update('projects');
		if($q){

			 
			 return TRUE;
		}
		else{
			return FALSE;
		}
	}

	public function get_vendors(){

		$q=$this->db->get('vendors');
		if($q->num_rows()){

			 $row=$q->result();
			 return $row;
		}
		else{
			return FALSE;
		}
	}	
	



	public function get_comments(){

		$q=$this->db->select('*,comments.id as cid,questions.q_english as eng')
					->from('comments')
					->join('questions', 'comments.q_id = questions.id')
					->get();
		if($q->num_rows()){

			 $row=$q->result();
			//  print_r($row);
			// exit();
		
			 return $row;
		}
		else{
			return FALSE;
		}
	}

	public function allow_comments($c_id,$isAllow){
		
		$q=$this->db->update('comments', ['isAllow'=>$isAllow], "id = $c_id");
		
		// $q=$this->db->where('id', $c_id);
		// 		->db->update('comments',['isAllow'=>$isAllow]);
		return TRUE;
	
	}

	public function get_answers($id,$qid){

		$q=$this->db->get_where('answers',['answer'=>$id,'q_id'=>$qid]);
		if($q->num_rows()){

			 $row=$q->num_rows();
			 return $row;
		}
		else{
			return FALSE;
		}
	}	

	

	public function count_Allquestions(){

		$q=$this->db->get('questions');
		if($q->num_rows()){

			 $row=$q->num_rows();
			 return $row;
		}
		else{
			return FALSE;
		}
	}		

	public function count_Allcomments(){

		$q=$this->db->get('comments');
		if($q->num_rows()){

			 $row=$q->num_rows();
			 return $row;
		}
		else{
			return FALSE;
		}
	}			


}