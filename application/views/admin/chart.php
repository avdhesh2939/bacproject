	
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Charts</h1>
				
			</div>
		</div><!--/.row-->
		<div class="row">
			<div class="col-lg-12 panel-body">
		<div class="panel panel-default">
		<?php echo form_open("question/insert_question"); ?>
								<div class="col-md-6">
								<div class="form-group">
									<label>Select Options</label>
									<select id="questionOpt" name="questionOpt" class="form-control chartOption js-example-basic-single" value="">
										<option>Select</option>
										<?php foreach($row1 as $row){ ?>
										<option value="<?= $row->id; ?>"><?= $row->q_english; ?></option>
									<?php } ?>
									</select>
								</div>
								</div>
							<div class="col-md-12 col-mt-10" style="margin-top: 10px;">
							<!-- <button type="submit" class="btn btn-primary">Submit Button</button></div>	 -->
							</form>
						</div>
					</div>
				</div>
		<div class="row" id="mainChart" style="display:none">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">Bar Chart</div>
					<div class="panel-body">
						<div class="canvas-wrapper Cwrapper">
							
						</div>
					</div>
				</div>
			</div>
		</div><!--/.row-->		
	</div>	<!--/.main-->
	  
	<script>
		

		!function ($) {
		    $(document).on("click","ul.nav li.parent > a > span.icon", function(){          
		        $(this).find('em:first').toggleClass("glyphicon-minus");      
		    }); 
		    $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
	</script>	
</body>

</html>
