
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">	
	<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active">All Vendors</li>
			</ol>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">All Vendors</h1>
			</div>
		</div>
<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">All Vendors</div>
					<div class="panel-body">
						<?php echo $this->session->flashdata('delete'); ?>
						<?php echo $this->session->flashdata('update'); ?>
						<table id="vendorTable" data-toggle="table"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
						    <thead>
						    <tr>
						        <th data-align="right" >ID</th>
						        <th>Name</th>
						        <th>Email</th>
						        <th>Contact No</th>
						        <th>Datetime</th>

						    </tr>
						    </thead>
						    <tbody>
						    	<?php 
						    	$count=1;
						    	if(!empty($row)){
						    	foreach($row as $rows){ ?>
						    		<tr>
						    			<td><?php echo $count++; ?></td>
						    			<td><?php echo $rows->name; ?></td>
						    			<td><?php echo $rows->email; ?></td>
						    			<td><?php echo $rows->contact; ?></td>
						    			<td><?php echo $rows->datetime; ?></td>
						    			
						    		</tr>	
						    	<?php } } ?>
						    </tbody>
						</table>
					</div>
				</div>
			</div>
	</div>
</div>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" />


    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.3.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script type="text/javascript">
	$('#vendorTable').DataTable({
                dom: 'Bfrtip',
                buttons: [
                {
                    extend: 'excelHtml5',
                    text: 'Export To Excel',
                    className: 'exportExcel',
                   // style:'background-color:red',
                    filename: 'Test_Excel'
              
                }],
                searching: false, 
                paging: false, 
               info: false
            });
 $(".exportExcel").css({"border-radius": "0.50em"});
</script>
