
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">	
	<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active">All Projects</li>
			</ol>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">All Projects</h1>
			</div>
		</div>
<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">All Projects</div>
					<div class="panel-body">
						<?php echo $this->session->flashdata('delete'); ?>
						<?php echo $this->session->flashdata('update'); ?>
						<table data-toggle="table"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
						    <thead>
						    <tr>
						        <th data-align="right" >ID</th>
						        <th>Project Name</th>
						        <th>Category Name</th>
						        <th>Description</th>
						        <th>Link</th>
						        <th>Type</th>
						        <th>Datetime</th>
						        <th>Action</th>

						    </tr>
						    </thead>
						    <tbody>
						    	<?php 
						    	$count=1;
						    	if(!empty($row)){
						    	foreach($row as $rows){ ?>
						    		<tr>
						    			<td><?php echo $count++; ?></td>
						    			<td><?php echo $rows->name; ?></td>
						    			<td><?php echo $rows->cat_name; ?></td>
						    			<td><?php echo $rows->description; ?></td>
						    			<td><?php echo $rows->link; ?></td>
						    			<td><?php if($rows->type==0){echo 'Work in Progress';}else{echo 'Completed';}; ?></td>
						    			<td><?php echo $rows->d; ?></td>
						    			<td><a href="<?php echo base_url("bac_admin/main/edit_project/$rows->pid"); ?>"><button class="btn btn-warning">Edit</button></a>&nbsp;<a href="<?php echo base_url("bac_admin/main/delete_project/$rows->pid"); ?>"><button class="btn btn-danger">Delete</button></a></td>
						    		</tr>	
						    	<?php } } ?>
						    </tbody>
						</table>
					</div>
				</div>
			</div>
	</div>
</div>
