<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>BAC - Dashboard</title>

<?php
echo link_tag('assets/css/bootstrap.min.css');
echo link_tag('assets/css/datepicker3.css');
echo link_tag('assets/css/styles.css');

?>
<!--Icons-->
<script src="<?= base_url("assets/js/lumino.glyphs.js"); ?>"></script>
<script src="<?= base_url("assets/js/jquery-1.11.1.min.js");?>"></script>
</head>

<body>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><span>BAC</span>Admin</a>
			</div>
							
		</div><!-- /.container-fluid -->
	</nav>
		