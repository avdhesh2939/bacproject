<?php print_r($row); ?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active">Questions</li>
			</ol>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Update Question</h1>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">Questions Elements</div>
					<?php echo $this->session->flashdata('update'); ?>	
					<div class="panel-body">
						<div class="col-md-12">
							<?php echo form_open("question/insert_question"); ?>
								<!-- <div class="col-md-6">
								<div class="form-group">
									<label>Select Options</label>
									<select id="option" name="options" class="form-control" value="">
										<option>Select</option>
										<option>2</option>
										<option>3</option>
										<option>4</option>
									</select>
								</div>
								</div> -->
								<div id="questionForm" style="display:block">
								<div id="div_english" class="col-md-12">
								<div class="form-group" >
								<label>Question</label>
								<input type="text" name="q_english" id="q_english" class="form-control" required placeholder="English" value="<?php $this->set_value('hello'); ?>">
								</div>
								</div>
								<div id="div_hindi" class="col-md-12">
								<div class="form-group">
								<input type="text" name="q_hindi" id="q_hindi" class="form-control" required placeholder="Hindi">
								</div>
								</div>
								<div id="div_gujrati" class="col-md-12">
								<div class="form-group">
								<input type="text" name="q_gujarati" id="q_gujarati" class="form-control" required placeholder="Gujarati">
								</div>
								</div>
								<div id="div_urdu" class="col-md-12">
								<div class="form-group">
								<input type="text" name="q_urdu" id="q_urdu" class="form-control" required placeholder="Urdu">
								</div>
								</div>
								<div id="div_tamil" class="col-md-12">
								<div class="form-group">
								<input type="text" name="q_tamil" id="q_tamil" class="form-control" required placeholder="Tamil">
								</div>
								</div>								


								
								</div>
						</form>

					</div>
					</div>
				</div>
			</div><!-- /.col-->
		</div><!-- /.row -->
		
	</div><!--/.main-->