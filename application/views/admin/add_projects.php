
	
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active">Projects</li>
			</ol>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Projects</h1>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-8">
				<div class="panel panel-default">
					<div class="panel-heading">Add Projects</div>
					<?php echo $this->session->flashdata('success'); ?>	
					<div class="panel-body">
						<div class="col-md-8">
							<?php echo form_open_multipart("bac_admin/main/insert_project"); ?>
								
								<div id="questionForm">
								<div class="col-md-12">
								<div class="form-group">
								<label>Type</label>
								<select class="form-control" name="type" required>
									<option>Select</option>
									<option value="1">Completed</option>
									<option value="0">Work In Progress</option>
								</select><br>	
								<label>Category Name</label>
								<select class="form-control" name='cat_id'>
									<option>Select</option>
									<?php foreach($row as $r){ ?>
									<option value="<?php echo $r->id; ?>"><?php echo $r->cat_name; ?></option>
									<?php } ?>
								</select>
								</div>
								<label>Project Name</label>
								<div class="form-group">
								<input type="text" name="name" value="" id="name" class="form-control" required placeholder="Project Name">
								</div>
								<label>Description</label>
								<div class="form-group">
									<textarea name="description" value="" id="description" class="form-control" required placeholder="Description"></textarea>
								</div>
								<label>Link</label>
								<div class="form-group">
								<input type="text" name="link" value="" id="link" class="form-control" required placeholder="Link">
								</div>
								<div class="form-group">
									<label>Images</label>
									<input type="file" name="userfile[]" multiple="multiple">
								</div>
								</div>
								<div class="col-md-12 col-mt-10" style="margin-top: 10px;"><button type="submit" class="btn btn-primary">Submit</button></div>								


								
								</div>
						</form>

					</div>
					</div>
				</div>
			</div><!-- /.col-->
		</div><!-- /.row -->
		
	</div><!--/.main-->

	