
	
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active">Category</li>
			</ol>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Category</h1>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-8">
				<div class="panel panel-default">
					<div class="panel-heading">Add Category</div>
					<?php echo $this->session->flashdata('success'); ?>	
					<div class="panel-body">
						<div class="col-md-8">
							<?php echo form_open("bac_admin/main/insert_cat"); ?>
								
								<div id="questionForm">
								<div class="col-md-12">
								<div class="form-group">
								<input type="text" name="cat_name" value="" id="cat-name" class="form-control" required placeholder="Category Name">
								</div>
								</div>
								<div class="col-md-12 col-mt-10" style="margin-top: 10px;"><button type="submit" class="btn btn-primary">Submit</button></div>								


								
								</div>
						</form>

					</div>
					</div>
				</div>
			</div><!-- /.col-->
		</div><!-- /.row -->
		
	</div><!--/.main-->

	