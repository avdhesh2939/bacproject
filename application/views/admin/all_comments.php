
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">	
	<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active">All Comments</li>
			</ol>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">All Comments</h1>
			</div>
		</div>
<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">All Comments</div>
					<div class="panel-body">
						<?php echo $this->session->flashdata('allow'); ?>
						<table data-toggle="table"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
						    <thead>
						    <tr>
						        <th data-align="right" >ID</th>
						        <th>UserId</th>
						        <th>Question</th>
						        <th>Comments</th>
						        <th>Datetime</th>
						        <th>Action</th>

						    </tr>
						    </thead>
						    <tbody>
						    	<?php 
						    	$count=1;
						    	if($row){
						    	foreach($row as $rows){ ?>
						    		<tr>
						    			<td><?php echo $count++; ?></td>
						    			<td><?php echo $rows->user_id; ?></td>
						    			<td><?php echo $rows->eng; ?></td>
						    			<td><?php echo $rows->comment; ?></td>
						    			<td><?php echo $rows->datetime; ?></td>
						    			<td><?php if($rows->isAllow==0){ ?><a href="<?php echo base_url("question/allow_comment/1/$rows->cid"); ?>"><button class="btn btn-primary">Allow</button></a><a href="<?php echo base_url("question/allow_comment/2/$rows->cid"); ?>"><button class="btn btn-danger">Reject</button></a><?php } elseif($rows->isAllow==1){?>
						    			<span class="text-center" style="color:green">Allowed</span> <?php } elseif($rows->isAllow==2){?>
						    			<span class="text-center" style="color:red">Rejected</span><?php } ?>	
						    		</td>
						    		</tr>	
						    	<?php }
						    	} ?>
						    </tbody>
						</table>
					</div>
				</div>
			</div>
	</div>
</div>
