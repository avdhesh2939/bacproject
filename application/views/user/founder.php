<!DOCTYPE html>
<html>

<!-- Mirrored from t.commonsupport.com/borvel/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 13 Jan 2019 05:38:27 GMT -->
<head>
<meta charset="utf-8">
<title>BAC | Founder</title>
<!-- Stylesheets -->

<link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
<link rel="icon" href="images/favicon.png" type="image/x-icon">
<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>

<body>

<div class="page-wrapper">
 	
    <!-- Preloader -->
    <div class="preloader"></div>
 	
    <!-- Main Header / Header Style Two-->
    <!--End Main Header -->
    
    <!--Page Title-->
    <section class="page-title" style="background-image:url(<?= base_url('user_assets/images/background/12.jpg');?>)">
    	<div class="auto-container">
        	<h1>Founder</h1>
            <ul class="page-breadcrumb">
            	<li><a href="index-2.html">Home</a></li>
                <li>Founder</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Approach Section-->
    <section class="approach-section">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
                <!--Content Column-->
                <div class="content-column col-md-8 col-sm-12 col-xs-12">
                	<div class="inner-column">
                    	<div class="sec-title">
                        	<h2>Founder</h2>
                        </div>
                        <div class="styled-text">Mr. Binod Singh is the Founder of BAC Group and holds a B Sc. In mathematics who has scripted the success story prominently across 2 decades since its inception.
</div>
                        <div class="text">
                        	<p>From a modest history in Coating, Protective and Decorative Painting business, he made an impact in all his endeavors with valued business ethics and prompt services, caring for customer’s needs. The continued growth and success of BAC Group bear testimony to his zeal to succeed.
He epitomized the dauntless entrepreneurial spirit of a visionary always on the march to change the destiny of a company.
He challenged conventional wisdom in several areas. He is among the Indian businessman to handle nuclear power project and discover the vast untapped potential in painting industry and channelize it for the growth and development of industry. The corporate philosophy he followed was short, simple and succinct:
“Think big. Think differently.
The journey began in August 2000 with NTPC project atKroba Chhattisgarh as a sub-contractor of gammon India with 5 people. In 2007 Binu Arts and Colours was established and it is also approved by Nuclear Power projects.</p>
                        </div>
                    </div>
                </div>
                
                <!--Image Column-->
                <div class="image-column col-md-4 col-sm-12 col-xs-12">
                	<div class="inner-column">
                    	<div class="image">
                        	<img src="<?= base_url('user_assets/images/resource/approach.jpg');?>" alt="" />
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <!--End Approach Section-->
    
    <!--Fluid Section One-->
    
    <!--End Fluid Section One-->
    
    <!--Contruction Section-->
   
    <!--End Contruction Section-->
    
    <!--Testimonial Section-->
    
    <!--End Testimonial Section-->
    
    <!--Renovation Section-->
   
    <!--End Renovation Section-->
    
    <!--Main Footer-->
    
    <!--End Main Footer-->
    
</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-arrow-up"></span></div>

</body>

<!-- Mirrored from t.commonsupport.com/borvel/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 13 Jan 2019 05:38:40 GMT -->
</html>