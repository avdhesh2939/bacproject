
<html>

<!-- Mirrored from t.commonsupport.com/borvel/projects-4.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 13 Jan 2019 05:39:48 GMT -->
<head>
<meta charset="utf-8">
<title>Borvel HTML Template | Awards</title>
<!-- Stylesheets -->



<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>

<body>

<div class="page-wrapper">
 	
    <!-- Preloader -->
    <div class="preloader"></div>
 	
    <!-- Main Header / Header Style Two-->
   <!--End Main Header -->
    
    <!--Page Title-->
    <section class="page-title" style="background-image:url(<?= base_url('user_assets/images/background/12.jpg'); ?>)">
    	<div class="auto-container">
        	<h1>Awards</h1>
            <ul class="page-breadcrumb">
            	<li><a href="index-2.html">Home</a></li>
                <li>Awards</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Gallery Section-->
    <section class="gallery-section style-three">
    	<div class="auto-container">
        	<!--MixitUp Galery-->
            <div class="mixitup-gallery">
                
                <!--Filter-->
                <div class="filter-list row clearfix">
                
					<!--Gallery Item-->
                    <?php foreach($pro as $pros){ 
                    $arr=explode(",", $pros->img);
                    ?>
                    <div class="gallery-item mix All <?php echo $pros->cat_name; ?> col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="image">
                                <img src="<?php echo base_url($arr[0]); ?>" alt="" />
                                <div class="overlay-box">
                                	<a href="projectDetails/<?php echo $pros->pid; ?>" class="overlay-link"></a>
                                	<div class="content">
                                    	<h3><a href="projectDetails/<?php echo $pros->pid; ?>"><?php echo $pros->name; ?></a></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                  <?php } ?>
                
                </div>
            </div>
        </div>
    </section>
    <!--End Gallery Section-->
    
    <!--Main Footer-->
   
    <!--End Main Footer-->
    
</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-arrow-up"></span></div>


</body>

<!-- Mirrored from t.commonsupport.com/borvel/projects-4.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 13 Jan 2019 05:39:49 GMT -->
</html>