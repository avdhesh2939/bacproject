<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>BAC  | Registration</title>
<!-- Stylesheets -->


<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

</head>

<body>

<div class="page-wrapper">
 	
    <!-- Preloader -->
   <!--  <div class="preloader"></div>
 	 -->
    <!-- Main Header / Header Style Two-->
   <!--End Main Header -->
    
    <!--Page Title-->
    <section class="page-title" style="background-image:url(<?= base_url('user_assets/images/background/12.jpg');?>)">
    	<div class="auto-container">
        	<h1>Registration</h1>
            <ul class="page-breadcrumb">
            	<li><a href="index-2.html">Home</a></li>
                <li>Registration</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Contact Section-->
    <section class="contact-section">
    	<div class="auto-container">
        	<!--Sec Title-->
        	<div class="sec-title">
            	<h2>Registration</h2>
            </div>
        	<div class="row clearfix">
            	
                <!--Form Column-->
                <div class="form-column col-md-8 col-sm-12 col-xs-12">
                	<div class="inner-column">
                    	
                        <!--Contact Form-->
                        <div class="default-form contact-form">
                            <form method="post" action="registerVendor" id="contact-form">
                                <?php echo $this->session->flashdata('success'); ?>
                                <div class="row clearfix">
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="name" value="" placeholder="Your name" required>
                                    </div>
                                    
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="email" value="" placeholder="Your email address" required>
                                    </div>
                                    
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="contact" value="" placeholder="Phone number" required>
                                    </div>
                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                        <button type="submit" class="theme-btn btn-style-one">Submit now</button>
                                    </div>                                        
                                </div>
                            </form>
                        </div>
                        <!--End Contact Form-->
                        
                    </div>
                </div>
                
                <!--Map Column-->
               

                
            </div>
        </div>
    </section>
    <!--End Contact Section-->
    
    <!--Main Footer-->
    
    <!--End Main Footer-->
    
</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-arrow-up"></span></div>



</body>

<!-- Mirrored from t.commonsupport.com/borvel/contact.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 13 Jan 2019 05:43:24 GMT -->
</html>