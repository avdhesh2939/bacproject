
<!DOCTYPE html>
<html>

<!-- Mirrored from t.commonsupport.com/borvel/projects-detail.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 13 Jan 2019 05:39:49 GMT -->
<head>
<meta charset="utf-8">
<title>BAC | Projects Details</title>
<!-- Stylesheets -->


<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>

<body>

<div class="page-wrapper">
 	
    <!-- Preloader -->
    <div class="preloader"></div>
 	
    <!-- Main Header / Header Style Two-->
    <!--End Main Header -->
    
    <!--Page Title-->
    <section class="page-title" style="background-image:url(<?= base_url('user_assets/images/background/12.jpg');?>)">
    	<div class="auto-container">
        	<h1>Project Details</h1>
            <ul class="page-breadcrumb">
            	<li><a href="index-2.html">Home</a></li>
                <li>Project Details</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Project Single Section-->
    <section class="project-single-section">
    	<div class="auto-container">
        	<div class="sec-title">
            	<h2><?php echo $detail['name']; ?></h2>
            </div>
            
            <div class="row clearfix">
            	<div class="column col-md-6 col-sm-12 col-xs-12">
                	<h3>Project Description</h3>
                    <div class="text">
                    	<p><?php echo $detail['description']; ?></p>
                    </div>
                </div>
                <?php $j=explode(",", $detail['img']) ?>
                <div class="column col-md-6 col-sm-12 col-xs-12">
                    
                            <div class="image">
        <div class="rev_slider_wrapper fullwidthbanner-container"  id="rev_slider_two_wrapper1" data-source="gallery" style="max-width:100%;left:0">
            <div class="rev_slider fullwidthabanner" id="rev_slider_two" data-version="5.4.1" >
                <ul style="width: 50%;height: 60%">
                	<?php foreach($j as $key=>$value){ ?>

                        
                    
                    <li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1688" data-masterspeed="500" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="<?php echo base_url($j[$key]); ?>" data-title="Slide Title" data-transition="parallaxvertical">
                    <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="<?php echo base_url($j[$key]); ?>"> 
                    
                    </li>

                <?php } ?>
            
        </ul>
    </div>
</div>
</div>
<div class="column col-md-6 col-sm-12 col-xs-12">
                    <h3>Project Link</h3>
                    <div class="text">
                        <p><?php echo $detail['link']; ?></p>
                    </div>
                </div>
                </div>
            </div>
            
           
            
            <!--Project Complition Section-->
            
        </div>
    </section>
    <!--End Project Single Section-->
    
    <!--Main Footer-->
<!--End Main Footer-->
    
</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-arrow-up"></span></div>


</body>

<!-- Mirrored from t.commonsupport.com/borvel/projects-detail.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 13 Jan 2019 05:39:53 GMT -->
</html>
