<!DOCTYPE html>
<html>

<!-- Mirrored from t.commonsupport.com/borvel/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 13 Jan 2019 05:38:27 GMT -->
<head>
<meta charset="utf-8">
<title>BAC | Services</title>
<!-- Stylesheets -->

<link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
<link rel="icon" href="images/favicon.png" type="image/x-icon">
<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>

<body>

<div class="page-wrapper">
 	
    <!-- Preloader -->
    <div class="preloader"></div>
 	
    <!-- Main Header / Header Style Two-->
    <!--End Main Header -->
    
    <!--Page Title-->
    <section class="page-title" style="background-image:url(<?= base_url('user_assets/images/background/12.jpg');?>)">
    	<div class="auto-container">
        	<h1>Services</h1>
            <ul class="page-breadcrumb">
            	<li><a href="index-2.html">Home</a></li>
                <li>Services</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Approach Section-->
    <section class="approach-section">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
                <!--Content Column-->
                <div class="content-column col-md-8 col-sm-12 col-xs-12">
                	<div class="inner-column">
                    	<div class="sec-title">
                        	<h2>Services</h2>
                        </div>
                        <div class="styled-text">Being a multi-disciplinary Service provider, BAC offers a wide range of Protective & Decorative Coating, Instrumentation Coating, Engineering, Project/Man Power Management and Mechanical Engineering services. These include:
</div>
                        <div class="text">
                        	<p class="styled-text">
                                <ul class="list-style-four">
                                    <li>Construction</li>
                                    <li>Fabrication</li>
                                    <li>Erection</li>
                                    <li>Building construction</li>
                                    <li>Road Construction</li>
                                    <li>Machine foundation</li>
                                    <li>ROB</li>
                                    <li>heavy structure</li>
                                    <li>light structure</li>
                                    <li>PEB</li>
                                    <li>Boiler</li>
                                    <li>Equipment</li>
                                    <li>Surface Protection & Coatings</li>
                                </ul>
                        <div class="styled-text">We are authorized applicator for: -Kansai Nerolac Paints, Jotun Paints,Berger Paints,Asian Paints.</div>        <ul class="list-style-four">
                                    <li>Industrial painting</li>
                                    <li>Metalizing services</li>
                                    <li>Pickling</li>
                                    <li>Sand/Grit blasting for structure service</li>
                                    <li>Guniting</li>
                                    <li>Operation and maintenances</li>
                                    <li>Nondestructive testing</li>
                                    <li>Manpower supply and management</li>
                                    <li>Civil & Mechanical service & management</li>
                                    <li>Site supervision and management</li>
                                    <li>Quality Control</li>
                                </ul>              

                        </div>
                    </div>
                </div>
                
                <!--Image Column-->
                <div class="image-column col-md-4 col-sm-12 col-xs-12">
                	<div class="inner-column">
                    	<div class="image">
                        	<img src="<?= base_url('user_assets/images/resource/approach.jpg');?>" alt="" />
                        </div>
                    </div>
                </div>
                 
                
            </div>
        </div>
    </section>
    <!--End Approach Section-->
    
    <!--Fluid Section One-->
    
    <!--End Fluid Section One-->
    
    <!--Contruction Section-->
   
    <!--End Contruction Section-->
    
    <!--Testimonial Section-->
    
    <!--End Testimonial Section-->
    
    <!--Renovation Section-->
  
    <!--End Renovation Section-->
    
    <!--Main Footer-->
  
    <!--End Main Footer-->
    
</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-arrow-up"></span></div>

</body>

<!-- Mirrored from t.commonsupport.com/borvel/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 13 Jan 2019 05:38:40 GMT -->
</html>