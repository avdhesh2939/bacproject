<!DOCTYPE html>
<html>

<!-- Mirrored from t.commonsupport.com/borvel/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 13 Jan 2019 05:38:27 GMT -->
<head>
<meta charset="utf-8">
<title>BAC | Infrastructure</title>
<!-- Stylesheets -->

<link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
<link rel="icon" href="images/favicon.png" type="image/x-icon">
<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>

<body>

<div class="page-wrapper">
 	
    <!-- Preloader -->
    <div class="preloader"></div>
 	
    <!-- Main Header / Header Style Two-->
    <!--End Main Header -->
    
    <!--Page Title-->
    <section class="page-title" style="background-image:url(<?= base_url('user_assets/images/background/12.jpg');?>)">
    	<div class="auto-container">
        	<h1>Infrastructure</h1>
            <ul class="page-breadcrumb">
            	<li><a href="index-2.html">Home</a></li>
                <li>Infrastructure</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Approach Section-->
    <section class="approach-section">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
                <!--Content Column-->
                <div class="content-column col-md-8 col-sm-12 col-xs-12">
                	<div class="inner-column">
                    	<div class="sec-title">
                        	<h2>Infrastructure</h2>
                        </div>
                       
                        <div class="text">
                            <div class="column col-md-6 col-sm-6 col-xs-12">
                                <div class="styled-text">Chanel Partners:</div>
                                        <ul class="list">
                                            <li>Nerolac paints India</li>
                                            <li>Asian paints</li>
                                            <li>Berger</li>
                                            <li>Royal Marine</li>
                                            <li>Metalizing Export Company</li>
                                            <li>Rapid & Graco</li>
                                            <li>Premier Instruments</li>
                                            <li>Tirupati Electricals</li>
                                            <li>Rockwool</li>
                                            

                                        </ul>
                                    </div>
                        	
                            
                          <div class="column col-md-6 col-sm-6 col-xs-12">
                                <div class="styled-text">Our infrastructure facilities:</div>
                                        <ul class="list">
                                            <li>JCB -02 Nos</li>
                                            <li>Dumper - 02 Nos</li>
                                            <li>Tractor - 02 Nos</li>
                                            <li>Hydra - 02 Nos.</li>
                                            <li>Cement concrete mixers - 03 Nos</li>
                                            <li>Diesel compressors (50hp, 35hp) - 10 Nos. + Electric compressor – 7 Nos</li>
                                            <li>Welding machines (Submerged Arc Welding (SAW),Manual Metal Arc welding (MMA)) - 03 Nos</li>
                                            <li>Conveyor systems - 02 Nos</li>
                                            <li>Metallizing systems – 17 Sets.</li><br>
                                            

                                        </ul>
                                    </div>

                            <div class="column col-md-6 col-sm-6 col-xs-12">
                                <div class="styled-text">Plant & Machinery Workshop:</div>
                                        <ul class="list">
                                            <li>Welding machines - 4Nos</li>
                                            <li>Grinder machines – 4</li>
                                            <li>Lath Machine – 1Nos</li>
                                            <li>Rolling machine – 1Nos</li>
                                            <li>Magnetic drill machine – 1Nos</li>
                                            <li>Vertical drill machine -1Nos</li>
                                            <li>Saw machine for cutting– 1Nos</li>
                                            <li>Gas cutting sets– 3Nos</li>
                                            <li>Forklift – 1Nos</li>
                                            <li>Compressor-1Nos</li>
                                            <li>Hopper-1Nos</li>
                                            <li>Airless machines- 2Nos</li>

                                            

                                        </ul>
                                    </div>        
                           <div class="column col-md-6 col-sm-6 col-xs-12">
                                <div class="styled-text">Inspection Tools:</div>
                                        <ul class="list">
                                            <li>Alco meter</li>
                                            <li>Comparator (for measuring surface profile)</li>
                                            <li>Distance measuring meter (electronic)</li>
                                            <li>Versa meter</li>
                                            <li>Vernier Calipers</li>
                                            
                                        </ul>
                                    </div>     
                           
                        </div>
                    </div>
                </div>
                
                <!--Image Column-->
                <div class="image-column col-md-4 col-sm-12 col-xs-12">
                	<div class="inner-column">
                    	<div class="image">
                        	<img src="<?= base_url('user_assets/images/resource/approach.jpg');?>" alt="" />
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <!--End Approach Section-->
    
    <!--Fluid Section One-->
    
    <!--End Fluid Section One-->
    
    <!--Contruction Section-->
   
    <!--End Contruction Section-->
    
    <!--Testimonial Section-->
    
    <!--End Testimonial Section-->
    
    <!--Renovation Section-->
   
    <!--End Renovation Section-->
    
    <!--Main Footer-->
   
    <!--End Main Footer-->
    
</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-arrow-up"></span></div>


</body>

<!-- Mirrored from t.commonsupport.com/borvel/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 13 Jan 2019 05:38:40 GMT -->
</html>