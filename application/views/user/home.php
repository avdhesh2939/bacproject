
<!DOCTYPE html>
<html>

<!-- Mirrored from t.commonsupport.com/borvel/index-6.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 13 Jan 2019 05:37:01 GMT -->
<head>
<meta charset="utf-8">
<title>BAC | Homepage Six</title>
<!-- Stylesheets -->

<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

</head>

<body>

<div class="page-wrapper">
 	
    <!-- Preloader -->
    <div class="preloader"></div>
 	
    <!-- Main Header-->
   <!--End Main Header -->
    
    <!--Main Slider-->
    <section class="main-slider">
    	
        <div class="rev_slider_wrapper fullwidthbanner-container"  id="rev_slider_two_wrapper" data-source="gallery">
            <div class="rev_slider fullwidthabanner" id="rev_slider_two" data-version="5.4.1">
                <ul>
                	
                    <li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1688" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="<?= base_url('user_assets/images/main-slider/image-13.jpg'); ?>" data-title="Slide Title" data-transition="parallaxvertical">
                    <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="<?= base_url('user_assets/images/main-slider/image-13.jpg'); ?>"> 
                    
                    
                    
                   
                   
                    </li>
                    
                    <li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1689" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="<?= base_url('user_assets/images/main-slider/image-14.jpg'); ?>" data-title="Slide Title" data-transition="parallaxvertical">
                    <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="<?= base_url('user_assets/images/main-slider/image-14.jpg'); ?>"> 
                    
                    
                    <div class="tp-caption tp-resizeme" 
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingtop="[0,0,0,0]"
                    data-responsive_offset="on"
                    data-type="text"
                    data-height="none"
                    data-width="['560','700','700','480']"
                    data-whitespace="normal"
                    data-hoffset="['15','15','15','15']"
                    data-voffset="['140','140','120','100']"
                    data-x="['left','left','left','left']"
                    data-y="['middle','middle','middle','middle']"
                    data-textalign="['top','top','top','top']"
                    data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                    	
                    </div>
                    
                    </li>
                    
                </ul>
            </div>
        </div>
    </section>
    <!--End Main Slider-->
    
    <!--Services Section Four-->
    <section class="services-section-four">
    	<div class="auto-container">
        	<div class="sec-title">
            	<h2>Our Services</h2>
                <div class="text">Being a multi-disciplinary Service provider, BAC offers a wide range of
Protective & Decorative Coating, Instrumentation Coating, Engineering,
Project/Man Power Management and Mechanical Engineering services.
These include: </div>
            </div>
            <div class="four-item-carousel owl-carousel owl-theme">
            	
                <!--Services Block Two-->
                <div class="service-block-two">
                	<div class="inner-box">
                        <div class="flip-container" >
                            <div class="flipper">
                            	<div class="front">
                                    <div class="icon-box">
                                        <span class="icon"><img src="<?= base_url('user_assets/images/icons/icon-1.svg'); ?>" alt="" /></span>
                                    </div>
                                    <h3>Construction</h3>
                                    <div class="text">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a.Building construction <br>
                                                      b.Road Construction<br>
                                                      &nbsp;&nbsp;c.Machine foundation <br>
                                                      &nbsp;</div>
                                </div>
                                
                                <div class="back">
                                    <div class="overlay-box">
                                        <div class="overlay-inner">
                                        	<div class="overlay-content">
                                                <h4><a href="building-contruction.html">Costruction</a></h4>
                                                <div class="overlay-text">Building construction is the process of adding structure to property or construction of buildings. </div>
                                                <a href="<?= base_url('user/services');?>" class="read-more">Read more</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Services Block Two-->
                <div class="service-block-two">
                	<div class="inner-box">
                        <div class="flip-container" >
                            <div class="flipper">
                            	<div class="front">
                                    <div class="icon-box">
                                        <span class="icon"><img src="<?= base_url('user_assets/images/icons/icon-2.svg'); ?>" alt="" /></span>
                                    </div>
                                    <h3>Fabrication</h3>
                                    <div class="text">
                                                      a. heavy structure<br>
                                                       b. light structure<br>
                                                       c. ROB <br>
                                                       d. PEB </div>
                                </div>
                                
                                <div class="back">
                                    <div class="overlay-box">
                                        <div class="overlay-inner">
                                        	<div class="overlay-content">
                                                <h4><a href="building-renovation.html">Fabrication</a></h4>
                                                <div class="overlay-text">Lexcept to obtain some advantage from it? But who has any right to find fault with a man chooses to enjoy.</div>
                                                <a href="<?= base_url('user/services');?>" class="read-more">Read more</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Services Block Two-->
                <div class="service-block-two">
                	<div class="inner-box">
                        <div class="flip-container" >
                            <div class="flipper">
                            	<div class="front">
                                    <div class="icon-box">
                                        <span class="icon"><img src="<?= base_url('user_assets/images/icons/icon-3.svg'); ?>" alt="" /></span>
                                    </div>
                                    <h3>Erection</h3>
                                    <div class="text">a. Boiler<br>
                                                      b. Equipment<br>
                                                      &nbsp;<br>
                                                      &nbsp;
                                                        </div>
                                </div>
                                
                                <div class="back">
                                    <div class="overlay-box">
                                        <div class="overlay-inner">
                                        	<div class="overlay-content">
                                                <h4><a href="building-maintenance.html">Erection</a></h4>
                                                <div class="overlay-text">Boiler-A boiler is a closedvesselin whichwateror other fluids heated. The fluid does not necessarily boil.</div>
                                                <a href="<?= base_url('user/services');?>" class="read-more">Read more</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Services Block Two-->
                <div class="service-block-two">
                	<div class="inner-box">
                        <div class="flip-container" >
                            <div class="flipper">
                            	<div class="front">
                                    <div class="icon-box">
                                        <span class="icon"><img src="<?= base_url('user_assets/images/icons/icon-4.svg'); ?>" alt="" /></span>
                                    </div>
                                    <h3>Surface Protection & Coatings</h3>
                                    <div class="text">a. Industrial painting<br>
                                                    b. Metalizing services<br>
                                                    c. Pickling<br>
                                                   </div>
                                </div>
                                
                                <div class="back">
                                    <div class="overlay-box">
                                        <div class="overlay-inner">
                                        	<div class="overlay-content">
                                                <h4><a href="architecture.html">Surface Protection & Coatings</a></h4>
                                                <div class="overlay-text">Industrial painting-An industrial coating is a paint or coating defined by its protective, rather than its aesthetic properties, although it can provide both</div>
                                                <a href="<?= base_url('user/services');?>" class="read-more">Read more</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Services Block Two-->
                <div class="service-block-two">
                	<div class="inner-box">
                        <div class="flip-container" >
                            <div class="flipper">
                            	<div class="front">
                                    <div class="icon-box">
                                        <span class="icon"><img src="<?= base_url('user_assets/images/icons/icon-1.svg'); ?>" alt="" /></span>
                                    </div>
                                    <h3>Operation and maintenances</h3>
                                    <div class="text">a. Manpower supply and management<br>
                                        &nbsp;<br>
                                                        </div>
                                </div>
                                
                                <div class="back">
                                    <div class="overlay-box">
                                        <div class="overlay-inner">
                                        	<div class="overlay-content">
                                                <h4><a href="building-contruction.html">Operation and maintenances</a></h4>
                                                <div class="overlay-text">In order to fall under this category of manpower supply service, payment to be made by the company must be related to the number of laborers supplied..</div>
                                                <a href="<?= base_url('user/services');?>" class="read-more">Read more</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Services Block Two-->
                <div class="service-block-two">
                	<div class="inner-box">
                        <div class="flip-container" >
                            <div class="flipper">
                            	<div class="front">
                                    <div class="icon-box">
                                        <span class="icon"><img src="<?= base_url('user_assets/images/icons/icon-2.svg'); ?>" alt="" /></span>
                                    </div>
                                    <h3>Nondestructive testing</h3>
                                    <div class="text">a. Quality Control
                                    &nbsp;<br>
                                &nbsp;<br>
                            &nbsp;<br>
                        &nbsp;<br></div>
                                </div>
                                
                                <div class="back">
                                    <div class="overlay-box">
                                        <div class="overlay-inner">
                                        	<div class="overlay-content">
                                                <h4><a href="building-renovation.html">Nondestructive testing</a></h4>
                                                <div class="overlay-text">Lexcept to obtain some advantage from it? But who has any right to find fault with a man chooses to enjoy.</div>
                                                <a href="<?= base_url('user/services');?>" class="read-more">Read more</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Services Block Two-->
                
                
                <!--Services Block Two-->
               
                
            </div>
        </div>
    </section>
    <!--End Services Section Two-->
    
    <!--Video Section-->
    <section class="video-section style-two">
    	<div class="auto-container">
        	
        	<div class="inner-container">
                <div class="row clearfix">
                    
                    <!--Content Column-->
                    <div class="content-column col-md-6 col-sm-12 col-xs-12">
                        <div class="inner-column">
                            <h2>About Us</h2>
                            <div class="text">BAC Pvt Ltd was established in 2016 as a parental company to the subsidiary alliance named Tirupati Engineers, Binu arts and Colours,We have started our journey from 2000 in the name of Binu Arts &Colours with Industrial Structure & Painting job at NTPC Korba after successful and timely execution of First job appreciated by all team like NTPC officials, Paint Supplier (Asian Paints & Nerolac) and all small associated suppliers & members of our team.</div>
                           <!--  <ul class="list-style-two">
                            	<li>Expert & Professional Engineers</li>
                                <li>We are Award Winning Firm</li>
                                <li>Fully Satisfaction Guarantee</li>
                                <li>70 + Successfull Projects done</li>
                            </ul> -->
                        </div>
                    </div>
                    
                    <!--Video Column-->
                    <div class="video-column col-md-6 col-sm-12 col-xs-12">
                        <div class="inner-column">

                            <div class="video-box">
                                <figure class="image">
                                    <img src="<?= base_url('user_assets/images/resource/video-img.jpg'); ?>" alt="">
                                    <a href="https://www.youtube.com/watch?v=kxPCFljwJws" class="lightbox-image overlay-box"><span class="flaticon-arrow"></span></a>
                                </figure>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
		
        </div>
    </section>
    <!--End Video Section-->
    
    <!--Project Section Two-->
    <section class="project-sectio-two style-three">
    	<div class="auto-container">
        	<div class="sec-title light">
        		<h2>Our Projects</h2>
            </div>
        </div>
        
       	<!--Porfolio Tabs-->
        <div class="project-tab">
            <div class="auto-container">
            
                <div class="tab-btns-box">
                    <!--Tabs Header-->
                    <div class="tabs-header">
                        <ul class="product-tab-btns clearfix">
                          <!--   <li class="p-tab-btn active-btn" data-tab="#p-tab-1">All</li> -->
                            <?php // foreach($row as $rows){ ?>
                           <!--  <li class="p-tab-btn" data-tab="#p-tab-2"><?php //echo $rows->cat_name; ?></li> -->
                        <?php // } ?>
                            
                        </ul>
                    </div>
                </div>
            
            	<!--Tabs Content-->  
                <div class="p-tabs-content">
                    <!--Portfolio Tab / Active Tab-->
                    <div class="p-tab active-tab" id="p-tab-1">
                        <div class="project-carousel-two owl-theme owl-carousel">
                             <?php foreach($row1 as $rows1){ 
                                $i=explode(',',$rows1->img);
                                $type=$rows1->type;
                               foreach($i as $key=>$value){
                                ?>
                            <!--Gallery Item-->
                            <div class="gallery-item">
                                <div class="inner-box">
                                    <div class="image">
                                        <img src="<?= base_url($value); ?>" alt="" />
                                        <div class="overlay-box">
                                	<a href="user/projectDetails/<?php echo $rows1->pid; ?>" class="overlay-link"></a>
                                	<div class="content">
                                        <?php if($type==1){ ?>
                                    	<h3><a href="user/projectDetails/<?php echo $rows1->pid; ?>">Completed</a></h3>
                                    <?php }else{ ?>
                                        <h3><a href="user/projectDetails/<?php echo $rows1->pid; ?>">Work In Progress</a></h3>
                                   <?php } ?>
                                    </div>
                                </div>
                                    </div>
                                </div>
                            </div>
                        <?php } } ?>
                            
                            <!--Gallery Item-->
                            
                            
                        </div>
                    </div>
                    
                    <!--Portfolio Tab-->
                    
                    <!--Portfolio Tab-->
                   
                    <!--Portfolio Tab-->
                   
                    <!--Portfolio Tab-->
                    
                    <!--Portfolio Tab-->
                    
                    <!--Portfolio Tab-->
                    
                </div>
            </div>
        </div>
        
    </section>
    <!--End Project Section-->
    
    <!--Contruction Section-->
    <section class="contruction-section" style="background-image:url(<?= base_url('user_assets/images/background/3.png)'); ?>">
    	<div class="auto-container">
        	<div class="row clearfix">
            	<!--Image Column-->
                <div class="image-column col-md-4 col-sm-12 col-xs-12">
                	<div class="image">
                    	<img src="<?= base_url('user_assets/images/resource/service-3.jpg'); ?>" alt="" />
                    </div>
                </div>
                <!--Content Column-->
                <div class="content-column col-md-8 col-sm-12 col-xs-12">
                	<div class="inner-column">
                    	<h2><span class="theme_color"> Founder</span> BAC Group</h2>
                        <div class="text">
                        	<p>Mr. Binod Singh is the Founder of BAC Group and holds a B Sc. In mathematics who has scripted the success story prominently across 2 decades since its inception. From a modest history in Coating, Protective and Decorative Painting business, he made an impact in all his endeavors with valued business ethics and prompt services, caring for customer’s needs. The continued growth and success of BAC Group bear testimony to his zeal to succeed..</p>
                        </div>
                        <a href="about.html" class="theme-btn btn-style-two">Know more about us</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End Contruction Section-->
    
    <!--Call To Action Section-->
    
    <!--Call End To Action Section-->
    
    <!--Testimonial Section-->
    
    <!--End Testimonial Section-->
    
    <!--News Section-->
   
    <!--End News Section-->
    
    <!--Renovation Section-->
  
    <!--End Renovation Section-->
    
    <!--Main Footer-->
   
    
</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-arrow-up"></span></div>


</body>

<!-- Mirrored from t.commonsupport.com/borvel/index-6.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 13 Jan 2019 05:37:25 GMT -->
</html>