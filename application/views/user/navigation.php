 <header class="main-header header-style-six">
        
        <!--Header-Upper-->
        <div class="header-upper">
            <div class="auto-container clearfix">
                
                <div class="pull-left logo-outer">
                    <div class="logo"><a href="<?= base_url('user'); ?>"><img src="<?= base_url('user_assets/images/logoMain.png'); ?>" alt="" title="" height='50' width='150'></a></div>
                </div>
                    
                <div class="pull-right upper-right clearfix">
                    
                    <!--Info Box-->
                    <div class="upper-column info-box">
                        <div class="icon-box"><span class="flaticon-technology-1"></span></div>
                        <ul>
                            <li>(1800) 456 7890 <span>Call us</span></li>
                        </ul>
                    </div>
                    
                    <!--Info Box-->
                    <div class="upper-column info-box">
                        <div class="icon-box"><span class="flaticon-location-pin"></span></div>
                        <ul>
                            <li>587 E,  Greenville Avenue <span>California, TX 70240</span></li>
                        </ul>
                    </div>
                    
                    <!--Info Box-->
                  <!--   <div class="upper-column info-box">
                        <a href="contact.html" class="theme-btn btn-style-five">Get appointment</a>
                    </div> -->
                    
                </div>
                
            </div>
        </div>
        <!--End Header Upper-->
        
        <!--Header Lower-->
        <div class="header-lower clearfix">
            <div class="auto-container">
                <div class="nav-outer clearfix">
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-header">
                            <!-- Toggle Button -->      
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            </button>
                        </div>
                        
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                 <li><a href="<?= base_url('user'); ?>">Home</a></li>
                                <li><a href="<?= base_url('user/aboutus');?>">About Us</a></li>
                                <li><a href="<?= base_url('user/founder');?>">Founder</a></li>
                                <li><a href="<?= base_url('user/project');?>">Projects</a></li>
                                <li><a href="<?= base_url('user/services');?>">Services</a></li>
                                <li><a href="<?= base_url('user/awards');?>">Awards</a></li>
                                 <li><a href="<?= base_url('user/introduction');?>">Introduction</a></li>
                                 <li><a href="<?= base_url('user/infrastructure');?>">Infrastructure</a></li>
                                 <li><a href="<?= base_url('user/contact');?>">Contact us</a></li>
                                 <li><a href="<?= base_url('user/register');?>">Register</a></li>

                             </ul>
                        </div>
                    </nav>
                    
                    <!--Search Box-->
                   
                    
                    <!-- Main Menu End-->
                   
                </div>
            </div>
        </div>
        <!--End Header Lower-->
        
        <!--Sticky Header-->
        <div class="sticky-header">
            <div class="auto-container">
                <div class="sticky-inner-container clearfix">
                    <!--Logo-->
                    <div class="logo pull-left">
                        <a href="<?= base_url('user'); ?>" class="img-responsive"><img src="<?= base_url('user_assets/images/logoMain.png'); ?>" alt="" title="" width='150'></a>
                    </div>
                    
                    <!--Right Col-->
                    <div class="right-col pull-right">
                        <!-- Main Menu -->
                        <nav class="main-menu">
                            <div class="navbar-header">
                                <!-- Toggle Button -->      
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            
                            <div class="navbar-collapse collapse clearfix">
                                <ul class="navigation clearfix">
                                 <li><a href="<?= base_url('user'); ?>">Home</a></li>
                                <li><a href="<?= base_url('user/aboutus');?>">About Us</a></li>
                                <li><a href="<?= base_url('user/founder');?>">Founder</a></li>
                                <li><a href="<?= base_url('user/project');?>">Projects</a></li>
                                <li><a href="<?= base_url('user/services');?>">Services</a></li>
                                 <li><a href="<?= base_url('user/introduction');?>">Introduction</a></li>
                                 <li><a href="<?= base_url('user/infrastructure');?>">Infrastructure</a></li>
                                 <li><a href="<?= base_url('user/contact');?>">Contact us</a></li>
                                  <li><a href="<?= base_url('user/register');?>">Register</a></li>
                                  
                                </ul>
                            </div>
                        </nav>
                        <!-- Main Menu End-->
                        
                        <!--Outer Box-->
                      
                    </div>
                    
                </div>
            </div>
        </div>
        <!--End Sticky Header-->
    
    </header>
    