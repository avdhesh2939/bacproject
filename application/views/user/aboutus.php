<!DOCTYPE html>
<html>

<!-- Mirrored from t.commonsupport.com/borvel/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 13 Jan 2019 05:38:27 GMT -->
<head>
<meta charset="utf-8">
<title>BAC | About Us</title>
<!-- Stylesheets -->

<link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
<link rel="icon" href="images/favicon.png" type="image/x-icon">
<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>

<body>

<div class="page-wrapper">
 	
    <!-- Preloader -->
    <div class="preloader"></div>
 	
    <!-- Main Header / Header Style Two-->
    <!--End Main Header -->
    
    <!--Page Title-->
    <section class="page-title" style="background-image:url(<?= base_url('user_assets/images/background/12.jpg');?>)">
    	<div class="auto-container">
        	<h1>About Us</h1>
            <ul class="page-breadcrumb">
            	<li><a href="index-2.html">Home</a></li>
                <li>About Us</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Approach Section-->
    <section class="approach-section">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
                <!--Content Column-->
                <div class="content-column col-md-8 col-sm-12 col-xs-12">
                	<div class="inner-column">
                    	<div class="sec-title">
                        	<h2>Company Approach</h2>
                        </div>
                        <div class="styled-text">BAC Pvt Ltd was established in 2016 as a parental company to the subsidiary alliance named Tirupati Engineers, Binu arts and Colours,
</div>
                        <div class="text">
                        	<p>We have started our journey from 2000 in the name of Binu Arts &Colours with Industrial Structure & Painting job at NTPC Korba after successful and timely execution of First job appreciated by all team like NTPC officials, Paint Supplier (Asian Paints & Nerolac) and all small associated suppliers & members of our team.
With all above facts we boost / encourage and work hard to enhance our business in other area / regions and expanded our business in Gujarat, Maharashtra, UP, Bihar, Chhattisgarh, Haryana.
Executed prestigious Projects at Dodsal -NPCIL-KAPP-3&4 with our team spirit and efforts and support from clients & suppliers and our continuous efforts, we have also incorporated many techniques, Equipment’s and technology has really help us to execute the quality job in required time frame. Such as specialty chemical tank painting, metallizing.
With all co-ordinations and collateral activities given an opportunity to work on Supply and Structural Fabrication and Piping work and with our positive attitude and good team we entered into the field of Site Fabrication of Storage Tanks, Piping & Structure Fabrication.
In-view of regular fabrication requirements we set up our Fabrication Workshop at SURAT – GUJARAT.
Certainly desire, efforts and responses giving us continuous opportunities to add new fields in our era.
OUR VISION: - To build, enhance BAC family providing employment to maximum possible personals from its present strength of 550 with safe, healthy and work acholic environment.
Subsidiaries of BAC Pvt Ltd are:
1. Binu Arts and Colours
2. Tirupati Engineers</p>
                        </div>
                    </div>
                </div>
                
                <!--Image Column-->
                <div class="image-column col-md-4 col-sm-12 col-xs-12">
                	<div class="inner-column">
                    	<div class="image">
                        	<img src="<?= base_url('user_assets/images/resource/approach.jpg');?>" alt="" />
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <!--End Approach Section-->
    
    <!--Fluid Section One-->
    
    <!--End Fluid Section One-->
    
    <!--Contruction Section-->
   
    <!--End Contruction Section-->
    
    <!--Testimonial Section-->
    
    <!--End Testimonial Section-->
    
    <!--Renovation Section-->
  
    <!--End Renovation Section-->
    
    <!--Main Footer-->
   
    <!--End Main Footer-->
    
</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-arrow-up"></span></div>



</body>

<!-- Mirrored from t.commonsupport.com/borvel/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 13 Jan 2019 05:38:40 GMT -->
</html>