<?php 

class User extends CI_Controller
{

public function index(){

	$this->load->model('usermodel');
	$row=$this->usermodel->get_category();
	$row1=$this->usermodel->get_projects();

	$this->load->view('user/header');
	$this->load->view('user/navigation');
	$this->load->view('user/home',['row'=>$row,'row1'=>$row1]);
	$this->load->view('user/footer');

}

public function aboutus(){
	$this->load->view('user/header');
	$this->load->view('user/navigation');
	$this->load->view('user/aboutus');
	$this->load->view('user/footer');


}

public function founder(){
	$this->load->view('user/header');
	$this->load->view('user/navigation');
	$this->load->view('user/founder');
	$this->load->view('user/footer');


}
	
public function services(){
	$this->load->view('user/header');
	$this->load->view('user/navigation');
	$this->load->view('user/services');
	$this->load->view('user/footer');


}	

public function introduction(){
	$this->load->view('user/header');
	$this->load->view('user/navigation');
	$this->load->view('user/intro');
	$this->load->view('user/footer');


}

public function infrastructure(){
	$this->load->view('user/header');
	$this->load->view('user/navigation');
	$this->load->view('user/infra');
	$this->load->view('user/footer');


}	

public function contact(){
	$this->load->view('user/header');
	$this->load->view('user/navigation');
	$this->load->view('user/contact');
	$this->load->view('user/footer');


}	

public function awards(){
	$this->load->model('usermodel');
	$cat=$this->usermodel->get_category();
	$pro=$this->usermodel->get_projects();
	$this->load->view('user/header');
	$this->load->view('user/navigation');
	$this->load->view('user/awards',['cat'=>$cat,'pro'=>$pro]);
	$this->load->view('user/footer');


}	

public function project(){
	$this->load->model('usermodel');
	$cat=$this->usermodel->get_category();
	$pro=$this->usermodel->get_projects();
	$this->load->view('user/header');
	$this->load->view('user/navigation');
	$this->load->view('user/project',['cat'=>$cat,'pro'=>$pro]);
	$this->load->view('user/footer');


}

public function projectDetails($id){
	$this->load->model('usermodel');
	$result=$this->usermodel->get_projectById($id);
	$this->load->view('user/header');
	$this->load->view('user/navigation');
	$this->load->view('user/projects_details',['detail'=>$result]);
	$this->load->view('user/footer');


}		

public function query(){
	
	$this->load->model('usermodel');
	
	$row=$this->input->post();
	unset($row['submit']);
	$result=$this->usermodel->submitQuery($row);
	if($result){

		$this->session->set_flashdata("success",'<div class="alert alert-success">
  <strong>Successfull!</strong>
</div>');
		redirect('user/contact');
	}else{
		$this->session->set_flashdata("success",'<div class="alert alert-danger">
  <strong>Error!</strong>.
</div>');
		redirect('user/contact');
	}


}	

public function register(){
	$this->load->view('user/header');
	$this->load->view('user/navigation');
	$this->load->view('user/registration');
	$this->load->view('user/footer');


}	

public function registerVendor(){
	
	$this->load->model('usermodel');
	
	$row=$this->input->post();
	unset($row['submit']);
	$result=$this->usermodel->registerVendor($row);
	if($result){

		$this->session->set_flashdata("success",'<div class="alert alert-success">
  <strong>Successfull!</strong>
</div>');
		redirect('user/contact');
	}else{
		$this->session->set_flashdata("success",'<div class="alert alert-danger">
  <strong>Error!</strong>.
</div>');
		redirect('user/register');
	}


}	
}

?>