<?php

Class Chart extends MY_Controller {

	public function __construct(){

			parent::__construct();
			if(!$this->session->userdata('id')){
			return redirect("login/index");
			}

			$this->load->model('loginmodel');
			

		}
	

	public function load_chart(){
		$this->load->view('login_header');
			$this->load->view('navigation');
		$row=$this->loginmodel->get_questions();
		$this->load->view('chart',['row1'=>$row]);
		$this->load->view('footer');
	}

	public function get_chart(){
		$id=$this->input->post('id');
		$row=$this->loginmodel->get_questionById($id);
		$opt=$row['options'];
		
		for($i=1;$i<=$opt;$i++){
			$row2=$this->loginmodel->get_answers($i,$id);
			$row["option$i"]=$row2;
		}
		echo json_encode($row);
		
	}

}








?>