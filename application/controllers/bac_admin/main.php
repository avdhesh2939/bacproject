<?php

class Main extends CI_Controller{

	public function __construct(){
				parent::__construct();
				if(!$this->session->userdata('id')){
					return redirect("login/index");
				}
				$this->load->model('loginmodel');
				$this->load->view('admin/login_header');
				$this->load->view('admin/navigation');


		}	
			
	public function addcategory(){

			$this->load->view('admin/add_category');
			$this->load->view('admin/footer');
		}

	public function insert_cat(){

			$row=$this->input->post();
			print_r($row);
			$result=$this->loginmodel->insert_category($row);
			if($result){
			$this->session->set_flashdata('success','<div class="alert bg-success" role="alert">
								<svg class="glyph stroked checkmark"><use xlink:href="#stroked-checkmark"></use></svg>Your Category Successfully Inserted.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a></div>');

			}else{
			$this->session->set_flashdata('success','<div class="alert bg-danger" role="alert">
								<svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg>Insertion Failed.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
							</div>');	
			}
			return redirect('bac_admin/main/addcategory');

		}

	public function getcategory(){


			$row=$this->loginmodel->get_category();
			// print_r($row);
			// exit();
			$this->load->view('admin/all_category',['row'=>$row]);
			$this->load->view('admin/footer');



		}

	public function edit_category($id){


				$row=$this->loginmodel->get_catById($id);
				$this->load->view('admin/edit_category',['row'=>$row]);
				$this->load->view('admin/footer');



			}


	public function update_cat(){

		$row=$this->input->post();
		$result=$this->loginmodel->update_catById($row);
		if($result){
			$this->session->set_flashdata('update','<div class="alert bg-success" role="alert">
								<svg class="glyph stroked checkmark"><use xlink:href="#stroked-cancel"></use></svg>Updated Successfully.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
							</div>');
		}
		return redirect("bac_admin/main/getcategory");



	}

	public function delete_category($id){

			$row=$this->loginmodel->delete_category($id);
			if($row){
				$this->session->set_flashdata('delete','<div class="alert bg-danger" role="alert">
								<svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg>Deleted Successfully.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
							</div>');
				return redirect("bac_admin/main/getcategory");
			}else{
				return redirect("bac_admin/main/getcategory");
			}
		}

	public function add_projects(){


			$row=$this->loginmodel->get_category();
			$this->load->view('admin/add_projects',['row'=>$row]);
			$this->load->view('admin/footer');



		}

	public function insert_project()
{       
	$row=$this->input->post();

    $imgs = [];
    $files = $_FILES;
    $cpt = count($_FILES['userfile']['name']);
    for($i=0; $i<$cpt; $i++)
    {
				$new_name = time()."-".$_FILES["userfile"]['name'][$i];
				$_FILES['file']['name']     = $new_name;
                $_FILES['file']['type']     = $_FILES['userfile']['type'][$i];
                $_FILES['file']['tmp_name'] = $_FILES['userfile']['tmp_name'][$i];
                $_FILES['file']['error']     = $_FILES['userfile']['error'][$i];
                $_FILES['file']['size']     = $_FILES['userfile']['size'][$i];
                
                $uploadPath = 'uploads/';
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                
                if($this->upload->do_upload('file')){
                    array_push($imgs, 'uploads/'.$this->upload->data('file_name'));
    }
    
}

			$imge=implode(',',$imgs);
			$row['img']=$imge;
			$result=$this->loginmodel->insert_project($row);
			if($result){
						$this->session->set_flashdata('success','<div class="alert bg-success" role="alert">
											<svg class="glyph stroked checkmark"><use xlink:href="#stroked-checkmark"></use></svg>Your Project Successfully Inserted.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a></div>');

						}else{
						$this->session->set_flashdata('success','<div class="alert bg-danger" role="alert">
											<svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg>Insertion Failed.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
										</div>');	
						}
						return redirect('bac_admin/main/add_projects');

}

	public function get_projects(){


				$row=$this->loginmodel->get_projects();
				// print_r($row);
				// exit();
				$this->load->view('admin/all_projects',['row'=>$row]);
				$this->load->view('admin/footer');



			}

	public function delete_project($id){

				$row=$this->loginmodel->delete_project($id);
				if($row){
					$this->session->set_flashdata('delete','<div class="alert bg-danger" role="alert">
									<svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg>Deleted Successfully.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
								</div>');
					return redirect("bac_admin/main/get_projects");
				}else{
					return redirect("bac_admin/main/get_projects");
				}
			}


	public function edit_project($id){


				$row=$this->loginmodel->get_projectById($id);
				$cat=$this->loginmodel->get_category();
				$this->load->view('admin/edit_project',['row'=>$row,'cat'=>$cat]);
				$this->load->view('admin/footer');



			}

	public function update_project(){

		$row=$this->input->post();
		$result=$this->loginmodel->update_projectById($row);
		if($result){
			$this->session->set_flashdata('update','<div class="alert bg-success" role="alert">
								<svg class="glyph stroked checkmark"><use xlink:href="#stroked-cancel"></use></svg>Updated Successfully.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
							</div>');
		}
		return redirect("bac_admin/main/get_projects");



	}

	public function get_vendors(){


				$row=$this->loginmodel->get_vendors();
				// print_r($row);
				// exit();
				$this->load->view('admin/all_vendors',['row'=>$row]);
				$this->load->view('admin/footer');



			}				
	
	public function get_comments(){


			$row=$this->loginmodel->get_comments();
			// print_r($row);
			// exit();
			$this->load->view('all_comments',['row'=>$row]);
			$this->load->view('footer');



		}

	public function allow_comment(){

			$c_id=$this->uri->segment('4');
			$isAllow=$this->uri->segment('3');
			$row=$this->loginmodel->allow_comments($c_id,$isAllow);
			if($row){
				$this->session->set_flashdata('allow','<div class="alert bg-danger" role="alert">
								<svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg>Action Finished Succesfully.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
							</div>');
				return redirect('question/get_comments');
			}



		}

	public function get_update_question($id){


			$row=$this->loginmodel->get_questionById($id);
			// print_r($row);
			// exit();
			$this->load->view('update_form',['row'=>$row]);
			$this->load->view('footer');



		}	

	
}
?>