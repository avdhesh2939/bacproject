<?php

Class Login extends CI_Controller {

	public function index()
	{
		if($this->session->userdata('id')){
			return redirect('bac_admin/login/dashboard');
		}
		$this->load->helper('form');
		$this->load->view('admin/header');
		$this->load->view('admin/login');
		$this->load->view('admin/footer');
	}

public function user_login()
	{
	
		$username=$this->input->post('username');
		$password=$this->input->post('password');
		$this->load->model('loginmodel');
		$result=$this->loginmodel->login_valid($username,$password);
		if($result==1){
		$this->session->set_userdata('id',$result);	
		return redirect('bac_admin/login/dashboard');
		}
		else {
		$this->session->set_flashdata('login_failed','Invalid Username/Password.');
		return redirect('login');
			}
}

public function dashboard()
	{
		$this->load->model('loginmodel');
		
		if(!$this->session->userdata('id')){
			return redirect('bac_admin/login');
		}
		$this->load->view('admin/login_header');
		$this->load->view('admin/navigation');
		$this->load->view('admin/index');
		$this->load->view('admin/footer');
	}

public function logout()
	{
		
		$this->session->unset_userdata('id');
		return redirect('bac_admin/login');
	}	
	
}







